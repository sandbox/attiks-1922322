<?php

/**
 * Print a single row of multiple fields.
 */
function theme_field_collection_table_multiple_value_field($variables) {
  $element = $variables['element'];
  $header = array();
  $cells = array();

  // Order field widgets by their widget weight.
  $instances = field_info_instances($element['#entity_type'], $element['#bundle']);
  uasort($instances, '_field_collection_table_sort_items_widget_helper');
  foreach ($instances as $field_name => $instance) {
    if (empty($element[$field_name])) {
      continue;
    }

    $header[] = _field_collection_table_get_title($element[$field_name]);
    $cells[] = array('data' => $element[$field_name]);

    // Remove the original field to prevent duplicate printing.
    unset($element[$field_name]);
  }

  $element['field_collection_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array($cells),
    '#weight' => 0,
  );

  $element['#sorted'] = FALSE;
  return drupal_render_children($element);
}

/**
 * Comparison function for sorting field instances by their widget's weight.
 */
function _field_collection_table_sort_items_widget_helper($a, $b) {
  $a_weight = (is_array($a) && isset($a['widget']['weight']) ? $a['widget']['weight'] : 0);
  $b_weight = (is_array($b) && isset($b['widget']['weight']) ? $b['widget']['weight'] : 0);
  return $a_weight - $b_weight;
}

/**
 * Replacement for theme_field_multiple_value_form().
 *
 * Each field is printed in a separate cell.
 */
function theme_field_collection_table_multiple_value_fields($variables) {
  $element = $variables['element'];
  $output = '';

  if (isset($element['#cardinality']) && ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED)) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $order_class = $element['#field_name'] . '-delta-order';
    $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

    $rows = array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_field_sort_items_value_helper');

    $header = array();
    // Add empty th for table dragging.
    if (!$element['#custom_settings']['nodragging']) {
      $header = array(array('data' => '', 'class' => ''));
    }

    // Keep track if headers are set.
    $headers_set = FALSE;

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      if (!isset($item['#entity_type']) || $item['#entity_type'] !== 'field_collection_item') {
        // Not a field collection item.
        continue;
      }
      uasort($item, 'element_sort');
      $item['_weight']['#attributes']['class'][] = $order_class;

      $cells = array();
      // Add classes for dragging if needed.
      if (!$element['#custom_settings']['nodragging']) {
        $cells = array(
          array('data' => '', 'class' => array('field-multiple-drag')),
        );
      }

      foreach (element_children($item) as $count => $field_name) {
        // Don't add the _weight
        if (!$element['#custom_settings']['nodragging'] || $field_name != '_weight') {
          // Only add the header once.
          if (!$headers_set) {
            $header[] = array(
              'data' => '<label>' . t('!title', array('!title' => _field_collection_table_get_title($item[$field_name]))) . '</label>',
              'class' => array('field-label'),
            );
          }
          $cells[] = array('data' => $item[$field_name]);
        }
      }

      // Mark headers as set if we have real children.
      if (!$headers_set) {
        $headers_set = count(element_children($item)) > 1;
      }

      // Mark rows as draggable if needed.
      if (!$element['#custom_settings']['nodragging']) {
        $rows[] = array(
          'data' => $cells,
          'class' => array('draggable'),
        );
      }
      else {
        $rows[] = array(
          'data' => $cells,
        );
      }
    }

    $output = array(
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
    );
    $output['label'] = array(
      '#prefix' => '<label class="field-label">',
      '#suffix' => '</label>',
      '#markup' => t('!title !required', array('!title' => $element['#title'], '!required' => $required)),
    );
    $output['field_collection_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#weight' => 20,
      '#attributes' => array(
        'id' => $table_id,
        'class' => array(
          'field-multiple-table',
        ),
      ),
    );
    if (!empty($element['#description'])) {
      $output[] = array(
        '#prefix' => '<div class="description">',
        '#suffix' => '</div>',
        '#markup' => $element['#description'],
        '#weight' => 30,
      );
    }
    if (isset($add_more_button)) {
      $btn = $add_more_button + array(
        '#prefix' => '<div class="clearfix">',
        '#suffix' => '</div>',
      );
      $btn['#weight'] = 40;
      $output[] = $btn;
    }

    $output = drupal_render($output);

    // Add table drag magic.
    if (!$element['#custom_settings']['nodragging']) {
      drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
    }
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }

  return $output;
}

/**
 * Implements template_preprocess_HOOK__PATTERN().
 */
function template_preprocess_table__field_collection_table(&$variables) {
  if (empty($variables['settings'])) {
    return;
  }
  if (isset($variables['settings']['empty'])) {
    _field_collection_table_hide_empty($variables);
  }
}

/**
 * Remove columns that are entirely empty.
 */
function _field_collection_table_hide_empty(&$variables) {
  $rows = $variables['rows'];

  $count = array();
  foreach ($rows as $row_delta => $row) {
    foreach ($row['data'] as $column_delta => $column) {
      if (!isset($count[$column_delta])) {
        $count[$column_delta] = 0;
      }
      if (isset($column['data']['#empty'])) {
        $count[$column_delta]++;
      }
    }
  }
  foreach ($count as $column_delta => $column) {
    if ($column === count($rows)) {
      foreach ($rows as $row_delta => $row) {
        unset($variables['rows'][$row_delta]['data'][$column_delta]);
        unset($variables['header'][$column_delta]);
      }
    }
  }
}

/**
 * Derivative of theme_table() solely for the HOOK_preprocess_table__PATTERN().
 */
function theme_table__field_collection_table($variables) {
  return theme_table($variables);
}

/**
 * Helps find the title of the field, as it could be in several places.
 */
function _field_collection_table_get_title($field) {
  $title = '';
  if (isset($field['#language']) && isset($field[$field['#language']])) {
    $language = $field['#language'];
    // Fix for integer fields.
    if (isset($field[$language][0]['value']['#title'])) {
      $title = $field[$language][0]['value']['#title'];
    }
    elseif (isset($field[$language][0]['#title'])) {
      $title = $field[$language][0]['#title'];
    }
    elseif (isset($field[$language]['#title'])) {
      $title = $field[$language]['#title'];
    }
  }
  elseif (isset($field['#title'])) {
    $title = empty($field['#is_weight']) ? $field['#title'] : t('Order');
  }
  elseif (isset($field['#value'])) {
    $title = $field['#value'];
  }

  return $title;
}
